# zykrr_flutter

A supporting core library for Zykrr survey forms.

# OS Version Support

| Android | iOS |
| ------ | ------ |
|SDK 19+ or 20+ |12.0+|

## Add zykrr_flutter as a dependency in your pubspec.yaml file

```dart
zykrr_flutter:
git:
url: https://gitlab.com/zykrr/sdks/flutter.git
ref: main
```


## Usage

```dart
          Navigator.push(
                context,
                        MaterialPageRoute(builder: (context) =>
                          ZykrrSurveyForm(
                            formBackgroundColor: Colors.white,
                            thankYouPageCloseTime:3,
                          viewEnum: ViewEnum.UNIQUE,
                          enableAppBar: true,
                        )),
                      );
```

## ZykrrSurveyForm Required Parameters

1. `formBackgroundColor` is a type of `Color`, This parameter is used to change color of the page background.

2. `thankYouPageCloseTime` is a type of `int`, This parameters used to automatically close the survey form after submitting.

3. `enableAppBar` is a type of `bool`, This parameter used to enable/disable the `ActionBar`.

   if `enableAppBar` is set 'true' then it will show the `ActionBar`of the survey form.

   if `enableAppBar` set 'false' then it will hide the `ActionBar` of the survey form.

4. `viewEnum` is a type of `ViewEnum`, There are two types of form supported by `zykrr_flutter`.

    1. ViewEnum.UNIQUE
    2. ViewEnum.OPEN


- `ViewEnum.UNIQUE`, if you pass type `ViewEnum.UNIQUE`, it will returun a unique form.

  In this type of view `userName`, `password` and `requestHeader` are the required parameters.
  `requestHeader` is a type of `Map<String, dynamic>`.
  `userName` is a type of `String`.
  `password` is a type of `password`.


- `ViewEnum.OPEN` if you pass the type `ViewEnum.OPEN`, You can give the direct form survey url link into the `directFormUrl` parameter.
  It will returun the survey form.

In this type of view `directFormUrl` parameters is required.

`directFormUrl` is a type of `String`.



## Description of the other parameters  ZykrrSurveyForm

Here

`formAppBarColor` is a type of `Color`, This parameter is used to change color of the Action bar .

`formAppBarTitle` is a type of `String`, This parameters used to change title of the Action bar.

`progressIndicatorColor` is a type of `Color`, This parameters used to change color of the Loader.

`closeButtonColor` is a type of `Color`, This parameters used to change color of the close button.

`closeButtonIcon` is a type of `IconData`, This parameters used to change Icon of the close button Icon.

`closeButtonHeight` is a type of `double`, This parameters used to change height of the close button.

`closeButtonWidth` is a type of `double`, This parameters used to change width of the close button.

`closeButtonPositionTop` is a type of `double`, This parameters used to change possion of the close button.
if `0` is set, then it will show close button on the top of the screen.

`closeButtonPositionBottom` is a type of `double`, This parameter is used to change form close button color.
if `0` is set, then it will show close button on the bottom of the screen.

`closeButtonPositionRight` is a type of `double`, This parameter is used to change form close button color.
if `0` is set, then it will show close button on the right of the screen.

`closeButtonPositionLeft` is a type of `double`, This parameter is used to change form close button color.
if `0` is set, then it will show close button on the left of the screen.

**Note: Close button Position will work only, when `enableAppBar` is set `false`**


`onPageStarted` is a type of `Map<String, dynamic> value`, This parameter is used to debug when page is stated loading.

It will returun the josn object like this  `{"message":"", "url":""}`.

In the `message` it will return the message and in the `url` it will return the which page stated.


`onPageFinished` is a type of `Map<String, dynamic> value`, This parameter is used to debug when page is finished after loading.

It will returun the josn object like this  `{"message":"", "url":""}`.

In the `message` it will return the API response message and in the `url` it will return the URL of the page.


`onUrlChange` is a type of `Map<String, dynamic> value`, This parameter is used to debug when page is url changed.

It will returun the josn object like this  `{"message":"", "url":""}`.

In the `message` it will return the API response message and in the `url` it will return the which page stated.


`onWebResourceError` is a type of `Map<String, dynamic> value`, This parameter is used to debug when page is giving some error.

It will returun the josn object like this  `{"message":"", "url":""}`.

In the `message` it will return the message and in the `url` it will return the which page stated.


`onSurveyPageClosed` is a type of `Map<String, dynamic> value`, This parameter is used to debug when the survey page is closed either by default timer or by clicking the close button.

It will returun the josn object like this  `{"message":"", "url":""}`.

In this case in the `message` object will get two type of message

1. THANKYOU_PAGE_CLOSED_BY_TIMER, it means it's closed by the timer.

2. SURVEY_CLOSED_BY_CLOSE_BUTTON , it means it's closed by the close button press.



## ZykrrSurveyForm Debugging CallBacks

 ```dart
  onPageStarted=(Map<String, dynamic> value){};

  onPageFinished=(Map<String, dynamic> value){};

  onUrlChange=(Map<String, dynamic> value){};

  onSurveyPageClosed=(Map<String, dynamic> value){};

  onWebResourceError=(Map<String, dynamic> value){};

  onProgress=(Map<String, dynamic> value){};
```
