class LoginResponse {
  LoginResponse({
    this.status,
    this.statusMessage,
    this.token,
  });

  final int? status;
  final String? statusMessage;
  String? token;

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      LoginResponse(
        status: json['status'],
        statusMessage: json['status_message'],
        token: json['token'],
      );

  Map<String, dynamic> toJson() => {
        'status': status,
        'status_message': statusMessage,
        'token': token,
      };
}