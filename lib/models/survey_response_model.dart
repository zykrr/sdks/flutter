class SurveyResponse {
  String? participantListId;
  List<String>? surveyLinks;
  SurveyResponse(
      {this.participantListId, this.surveyLinks});

  SurveyResponse.fromJson(Map<String, dynamic> json) {
    participantListId = json['participantListId'];
    if (json['surveyLinks'] != null) {
      surveyLinks = <String>[];
      json['surveyLinks'].forEach((v) {
        surveyLinks!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['participantListId'] = participantListId;
    if (surveyLinks != null) {
      data['surveyLinks'] =
          surveyLinks!.map((v) => v).toList();
    }
    return data;
  }
}