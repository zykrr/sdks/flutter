import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import '../common/api_constant.dart';
import '../common/dependencies_injection.dart';
import '../models/login_response_model.dart';
import '../models/survey_response_model.dart';

class ApiRepository {
  final Dio _dio;
  ApiRepository({
    required Dio dio,
  })  :_dio = dio;
  /// returns either type of error-msg or Unit-isSuccess
  Future<Either<String, String>> signInWithEmailPassword(
    String userName,
    String password,String url,
      Map<String, dynamic> webHeader,
      ) async {
    try {
      var headers = {
        'username': userName,
        'password': password
      };
      var response = await _dio.request(
        url,
        options: Options(
          method: 'POST',
          headers: headers,
        ),
      );
      if (response.statusCode==200) {
        // success
        String token=LoginResponse.fromJson(response.data).token??'';
        var headers = {
          'Cookie': 'zykrr=$token',
          'Content-Type': 'application/json'
        };
        var data = json.encode(
            webHeader);
        var res = await _dio.request(
          ApiConstant.surveyApi,
          options: Options(
            method: 'POST',
            headers: headers,
          ),
          data: data,
        );
        String webUrl='';
        if (res.statusCode == 200) {
          webUrl=SurveyResponse.fromJson(res.data).surveyLinks![0]??'';
        }
        else {
          kApiController.errorMessage.value=res.data.toString();
          if (kDebugMode) {
            print(res.statusMessage);
          }
        }
        return right(webUrl);
      } else {
        kApiController.errorMessage.value=response.data.toString();
        // failure
        if (kDebugMode) {
          print(response.statusMessage);
        }
        String? errorMessage;
        errorMessage ='error';
        return left(errorMessage ?? '');
      }
    } on DioException catch (e) {
      kApiController.errorMessage.value=e.response!.data.toString();
      return left(e.response!.data.toString());
    } catch (e, st) {
      // error
      kApiController.errorMessage.value=e.toString()??'Something went wrong!';
      return left('Something went wrong!');
    }
  }

}
