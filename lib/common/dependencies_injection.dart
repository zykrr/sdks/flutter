import 'package:get/get.dart';
import '../controller/api_controller.dart';
import '../services/api_repository.dart';
import 'api_dio.dart';

final ApiRepository kApiRepository =
    ApiRepository(dio: ApiDio.instance);
// auth-controller
final ApiController kApiController = Get.put(ApiController(kApiRepository));
