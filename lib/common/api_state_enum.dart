/// 3 state of auth
enum AuthState {
  initial,
  unAuthenticated,
  authenticated,
}
