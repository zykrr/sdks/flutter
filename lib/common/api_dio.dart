import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';

abstract class ApiDio {
  static final _dio = _getDio();

  static Dio get instance => _dio;

  static const _loggerName = ' Authentication ';

  static Dio _getDio() {
    final options = BaseOptions(
      baseUrl: '',
    );
    final interceptor = InterceptorsWrapper(
      onRequest: (request, handler) async {
        try {
          log(
            '${request.uri} || ${request.headers} || ${json.encode(request.data)}',
            name: _loggerName,
          );
        } catch (_) {}
        handler.next(request);
      },
      onResponse: (response, handler) {
        try {
          log(
            'STATUS: ${response.statusCode} | RESPONSE: ${json.encode(response.data)}',
            name: _loggerName,
          );
        } catch (_) {}
        handler.next(response);
      },
      onError: (error, handler) {
        log(
          'ERROR-TYPE: ${error.type} | ERROR ${error.error}',
          name: _loggerName,
        );
        handler.next(error);
      },
    );
    return Dio(options)..interceptors.add(interceptor);
  }
}
