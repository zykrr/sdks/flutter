import 'dart:ui';

import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'dart:async';
import 'package:webview_flutter/webview_flutter.dart';
// Import for Android features.
import 'package:webview_flutter_android/webview_flutter_android.dart';
// Import for iOS features.
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

import '../common/api_state_enum.dart';
import '../services/api_repository.dart';

class ApiController extends GetxController {
  ApiController(this._apiRepository);
  final ApiRepository _apiRepository;
  Rx<AuthState> authState = Rx(AuthState.initial);
  RxBool signingProgress = false.obs;
  RxString webUrl = ''.obs;
  RxString errorMessage = ''.obs;
  WebViewController? controller;

  Future<Either<String, Unit>> signInWithEmailPassword({
    required String userName,
    required String password,
    required String url,
    required Map<String, dynamic> webHeader
  }) async {
    signingProgress.value = true;
    final failOrSuccess = await _apiRepository.signInWithEmailPassword(
      userName,
      password,
      url,
      webHeader,
    );
    signingProgress.value = false;
    return failOrSuccess.fold(
      (l) {
        authState.value = AuthState.unAuthenticated;
      return left(l);
      },
      (r) {
        authState.value = AuthState.authenticated;
        webUrl.value=r;
        return right(unit);
      },
    );
  }

  void initWebView(BuildContext context,
      int thankYouPageCloseTime,
      Function(Map<String, dynamic>)? onPageStarted,
      Function(Map<String, dynamic>)? onPageFinished,
      Function(Map<String, dynamic>)? onUrlChange,
      Function(Map<String, dynamic>)? onWebResourceError,
      Function(Map<String, dynamic>)? onSurveyPageClosed,
      Function(Map<String, dynamic>)? onProgress) {
    // #docregion platform_features
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
    WebViewController.fromPlatformCreationParams(params);
    // #enddocregion platform_features

    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            final Map<String, dynamic> callBack={"message":"WebView is loading progress", "url":"$progress"};
            onProgress?.call(callBack);
            //debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            //debugPrint('Page started loading: $url');
            final Map<String, dynamic> callBack={"message":"Page started loading:", "url":"$url"};

            onPageStarted?.call(callBack);
          },
          onPageFinished: (String url) {
            final Map<String, dynamic> callBack={"message":"Page finished loading:", "url":"$url"};

            //debugPrint('Page finished loading: $url');
            onPageFinished?.call(callBack);
          },
          onWebResourceError: (WebResourceError error) {
            final Map<String, dynamic> callBack={"message":"Page resource error:", "url":"${error.description}"};
            onWebResourceError?.call(callBack);
          },
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              debugPrint('blocking navigation to ${request.url}');
              return NavigationDecision.prevent;
            }
            debugPrint('allowing navigation to ${request.url}');
            return NavigationDecision.navigate;
          },
          onUrlChange: (UrlChange change) {
            final Map<String, dynamic> callBack={"message":"Page onUrlChange:", "url":"${change.url}"};
            onUrlChange?.call(callBack);
            if(change.url!.contains('thankyou')){
              final time = Duration(seconds: thankYouPageCloseTime);
              Timer.periodic(
                time,
                    (Timer timer) {
                    Navigator.of(context).pop();
                    final Map<String, dynamic> callBack={"message":"THANKYOU_PAGE_CLOSED_BY_TIMER", "url":"${change.url}"};
                    onSurveyPageClosed?.call(callBack);
                    },
              );
            }
            //debugPrint('url change to ${change.url}');
          },

        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      );

    // #docregion platform_features
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }
    // #enddocregion platform_features
    this.controller = controller;
  }

}
