
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:zykrr_flutter/common/view_enum.dart';
import '../common/api_constant.dart';
import '../common/api_state_enum.dart';
import '../common/dependencies_injection.dart';
class ZykrrSurveyForm extends StatefulWidget {
  final Map<String, dynamic> requestHeader;
  final Color closeButtonColor;
  final Color progressIndicatorColor;
  final Color formBackgroundColor;
  final Color formAppBarColor;
  final Color errorTextColor;
  final Color errorIconColor;
  final IconData errorIcon;
  final String formAppBarTitle;
  final Color formAppBarTextColor;
  final int thankYouPageCloseTime;
  final double? closeButtonPositionTop;
  final double? closeButtonPositionRight;
  final double? closeButtonPositionLeft;
  final double? closeButtonPositionBottom;
  final double closeButtonHeight;
  final double closeButtonWidth;
  final ViewEnum viewEnum;
  final String directFormUrl;
  final bool enableAppBar;
  final IconData closeButtonIcon;
  final String userName;
  final String password;
   Function(Map<String, dynamic>)? onPageStarted=(Map<String, dynamic> value){};
   Function(Map<String, dynamic>)? onPageFinished=(Map<String, dynamic> value){};
   Function(Map<String, dynamic>)? onUrlChange=(Map<String, dynamic> value){};
   Function(Map<String, dynamic>)? onSurveyPageClosed=(Map<String, dynamic> value){};
   Function(Map<String, dynamic>)? onWebResourceError=(Map<String, dynamic> value){};
   Function(Map<String, dynamic>)? onProgress=(Map<String, dynamic> value){};
    ZykrrSurveyForm({super.key,
     this.requestHeader=const{},
     this.closeButtonColor=Colors.white,
    required this.formBackgroundColor,
     this.formAppBarColor=Colors.black45,
     this.formAppBarTitle='',
    required this.thankYouPageCloseTime,
    required this.viewEnum,
     this.directFormUrl='',
    required this.enableAppBar,
     this.formAppBarTextColor=Colors.white,
     this.closeButtonIcon=Icons.close,
     this.progressIndicatorColor=Colors.black45,
     this.userName='',
     this.password='',
     this.onPageStarted,
      this.onPageFinished,
      this.onUrlChange,
      this.onSurveyPageClosed,
      this.onWebResourceError,
     this.onProgress,
     this.errorTextColor=Colors.red,
     this.errorIconColor=Colors.red,
      this.errorIcon=Icons.error_outline_outlined,
      this.closeButtonPositionTop,
      this.closeButtonPositionRight,
      this.closeButtonPositionLeft,
      this.closeButtonPositionBottom,
      this.closeButtonHeight=24,
      this.closeButtonWidth=24,});

  @override
  State<ZykrrSurveyForm> createState() => _ZykrrSurveyFormState();
}

class _ZykrrSurveyFormState extends State<ZykrrSurveyForm> {
  @override
  void initState() {
    super.initState();
    kApiController.initWebView(context,
         widget.thankYouPageCloseTime,
         widget.onPageStarted,
         widget.onPageFinished,
         widget.onUrlChange,
         widget.onWebResourceError,
         widget.onSurveyPageClosed,
         widget.onProgress);
  }

  @override
  Widget build(BuildContext context) {
     if(widget.viewEnum==ViewEnum.OPEN) {
       if(widget.directFormUrl.isEmpty) {
         String errorMessage = 'directFormUrl field is required!';
         return _getErrorScaffold(errorMessage);
       }
       else {
         kApiController.controller!.clearCache();
         kApiController.controller!.loadRequest(
             Uri.parse(widget.directFormUrl));
         return _getSuccessScaffold();
       }
     }
       else if(widget.viewEnum==ViewEnum.UNIQUE){

       if(widget.userName.isEmpty){
         String errorMessage='userName field is required!';
         return _getErrorScaffold(errorMessage);
       }
       else if(widget.password.isEmpty){
         String errorMessage='password field is required!';
         return _getErrorScaffold(errorMessage);
       }
       else if(widget.requestHeader.isEmpty){
         String errorMessage='requestHeader field is required!';
         return _getErrorScaffold(errorMessage);
       }
       return FutureBuilder(
          future: kApiController.signInWithEmailPassword(
              userName: widget.userName, password: widget.password,
              url: ApiConstant.loginApi,
              webHeader: widget.requestHeader),
          builder: (context, snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return Scaffold(
                backgroundColor: widget.formBackgroundColor,
                appBar: widget.enableAppBar?_getActionBar(context):null,
                body: Container(
                  color: widget.formBackgroundColor,
                  child:  Center(child: CircularProgressIndicator(color: widget.progressIndicatorColor,)),),
              );
            }
            return Obx(
                  () {
                switch (kApiController.authState.value) {
                  case AuthState.initial:
                    return  Container(color: widget.formBackgroundColor,);
                  case AuthState.unAuthenticated:
                    return _getErrorScaffold(kApiController.errorMessage.value);
                  case AuthState.authenticated:
                    kApiController.controller!.clearCache();
                    kApiController.controller!.loadRequest(Uri.parse(kApiController.webUrl.value));
                    return _getSuccessScaffold();
                }
              },
            );
          },
        );
     }
       else{
        return _getErrorScaffold('Oops... something went wrong');
     }

  }

  _getErrorWidget(String errorMessage){
    return Padding(
      padding: const EdgeInsets.only(left: 16,right: 16),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children:  [
             Icon(
              widget.errorIcon,
              color: widget.errorIconColor,
              size: 100,
            ),
            const SizedBox(height: 16,),
            Text(
              errorMessage,
              style:  TextStyle(fontSize: 24,
              color: widget.errorTextColor
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
  _getErrorScaffold(String errorMessage){
    return Scaffold(
      backgroundColor: widget.formBackgroundColor,
      appBar: widget.enableAppBar?_getActionBar(context):null,
      body: widget.enableAppBar?_getErrorWidget(errorMessage)
          :Stack(children: [
        _getErrorWidget(errorMessage),
        Positioned(
            top: widget.closeButtonPositionTop,
            bottom:widget.closeButtonPositionBottom ,
            right: widget.closeButtonPositionRight,
            left:widget.closeButtonPositionLeft ,
            child:_getActionBar(context)),
      ]),
    );
  }

  _getSuccessScaffold(){
    return Scaffold(
      //extendBodyBehindAppBar:true,
      backgroundColor: widget.formBackgroundColor,
      appBar: widget.enableAppBar?_getActionBar(context):null,
      body: widget.enableAppBar?WebViewWidget(controller: kApiController.controller!)
          :Stack(children: [
        WebViewWidget(controller: kApiController.controller!),
        Positioned(
            top: widget.closeButtonPositionTop,
            bottom:widget.closeButtonPositionBottom ,
            right: widget.closeButtonPositionRight,
            left:widget.closeButtonPositionLeft,
            child:_getActionBar(context)),
      ]),
    );
  }
  _getActionBar(BuildContext context){
    if(widget.enableAppBar) {
      return AppBar(
        backgroundColor: widget.formAppBarColor,
        automaticallyImplyLeading: false,
        title: Text(widget.formAppBarTitle,
        style: TextStyle(color: widget.formAppBarTextColor),
        ),
        actions: [
          MaterialButton(
            height: widget.closeButtonHeight,
            minWidth: widget.closeButtonWidth,
            elevation: 0,
            onPressed: () {
              Navigator.of(context).pop();
              final Map<String, dynamic> callBack={"message":"SURVEY_CLOSED_BY_CLOSE_BUTTON", "url":""};
              widget.onSurveyPageClosed?.call(callBack);
            },
            color: widget.closeButtonColor.withOpacity(0.1),
            textColor: widget.closeButtonColor,
            padding: const EdgeInsets.all(4),
            shape: const CircleBorder(),
            child:  Icon(
              widget.closeButtonIcon,
              color: widget.closeButtonColor,
              size: widget.closeButtonHeight,
            ),
          )
        ],
      );
    }
    else{
      return MaterialButton(
        height: widget.closeButtonHeight,
        minWidth: widget.closeButtonWidth,
        elevation: 0,
        onPressed: () {
          final Map<String, dynamic> callBack={"message":"SURVEY_CLOSED_BY_CLOSE_BUTTON", "url":""};
          widget.onSurveyPageClosed?.call(callBack);
          Navigator.of(context).pop();
        },
        color: widget.closeButtonColor.withOpacity(0.1),
        textColor: widget.closeButtonColor,
        padding: const EdgeInsets.all(4),
        shape: const CircleBorder(),
        child:  Icon(
          widget.closeButtonIcon,
          color: widget.closeButtonColor,
          size: widget.closeButtonHeight,
        ),
      );
    }

  }

}